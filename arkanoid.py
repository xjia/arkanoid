#!/usr/bin/python

import pygame, time, math
from pygame.locals import *
from random import randrange

def main():
	pygame.init()
	
	size = width, height = 800, 600
	black = 0, 0, 0
	
	brick_size = brick_width, brick_height = 80, 22
	ball_size = ball_width, ball_height = 20, 20
	bat_size = bat_width, bat_height = 200, 17
	
	radius = 10
	
	bat_pos = [width / 2, height - bat_height / 2]
	bat_speed = [0, 0]
	bat_accel = [0, 0]

	ball_pos = [width / 2, height / 2]
	ball_speed = [0, 0]
	ball_accel = [0, 0.098]
	
	bricks = []

	brick_n = 10
	for i in range(brick_n):
		for j in range(brick_n):
			bricks.append([i, j, randrange(1, 4)])
	
	bat_ball_coef = 0.2
	bat_slow_coef = 0.995
	brick_fast_coef = 1.005
	
	def accum_vec(base, delta):
		return [base[0] + delta[0], base[1] + delta[1]]
	
	def is_alive(brick):
		return brick[2]
	
	screen = pygame.display.set_mode(size)
	pygame.display.set_caption('Bat Ball Brick')
	
	ibat = pygame.image.load('bat.png')
	batrect = ibat.get_rect()
	
	iball = pygame.image.load('ball.png')
	ballrect = iball.get_rect()
	
	ibrick = pygame.image.load('brick.png')
	brickrect = ibrick.get_rect()
	
	ibricks = [pygame.image.load('greenbrick.png'), pygame.image.load('redbrick.png'), ibrick]
	
	def get_brick_rect(brick):
		return brickrect.move(brick[0] * brick_width, brick[1] * brick_height)
	
	clock = pygame.time.Clock()
	
	def in_rect(p, l, r, t, b):
		return l <= p[0] <= r and t <= p[1] <= b
	
	def near(a, b, r):
		return (a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]) <= r*r
	
	# main loop
	while 1:
		# ensure FPS = 60
		clock.tick(60)
		
		# event handling
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				return
			elif event.type == pygame.KEYDOWN:
				if event.key == K_LEFT:
					if bat_speed[0] == 0:
						bat_speed[0] = -10
						bat_accel[0] = -0.5
					else:
						bat_speed[0] = bat_speed[0] + bat_accel[0]
				elif event.key == K_RIGHT:
					if bat_speed[0] == 0:
						bat_speed[0] = 10
						bat_accel[0] = 0.5
					else:
						bat_speed[0] = bat_speed[0] + bat_accel[0]
			elif event.type == pygame.KEYUP:
				if event.key == K_LEFT or event.key == K_RIGHT:
					bat_speed = [0, 0]
					bat_accel = [0, 0]
		
		# physics simulation
		bat_speed = accum_vec(bat_speed, bat_accel)
		bat_pos = accum_vec(bat_pos, bat_speed)
		
		if bat_pos[0] < bat_width / 2:
			bat_pos[0] = bat_width / 2
		if bat_pos[0] > width - bat_width / 2:
			bat_pos[0] = width - bat_width / 2
		
		ball_speed = accum_vec(ball_speed, ball_accel)
		ball_pos = accum_vec(ball_pos, ball_speed)
		
		if ball_pos[1] > height:
			# game over
			time.sleep(0.618)
			
			bat_pos = [width / 2, height - bat_height / 2]
			bat_speed = [0, 0]
			bat_accel = [0, 0]

			ball_pos = [width / 2, height / 2]
			ball_speed = [0, 0]
			ball_accel = [0, 0.098]

			bricks = []

			brick_n = 10
			for i in range(brick_n):
				for j in range(brick_n):
					bricks.append([i, j, randrange(1, 4)])
		elif ball_pos[1] < radius:
			ball_speed[1] = -ball_speed[1]
		
		if ball_pos[0] < radius:
			ball_speed[0] = -ball_speed[0]
			ball_pos[0] = radius
	 	elif ball_pos[0] > width - radius:
			ball_speed[0] = -ball_speed[0]
			ball_pos[0] = width - radius
		
		if ball_pos[1] + radius >= bat_pos[1] - bat_height / 2:
			ball_accel = [0, 0]
			if bat_pos[0] - bat_width / 2 <= ball_pos[0] <= bat_pos[0] + bat_width / 2:
				ball_speed[0] = ball_speed[0] + bat_speed[0] * bat_ball_coef
				ball_speed[1] = -ball_speed[1] * bat_slow_coef
				ball_pos[1] = bat_pos[1] - bat_height / 2 - radius
			elif bat_pos[0] - bat_width / 2 - radius < ball_pos[0] < bat_pos[0] - bat_width / 2:
				ball_speed[0] = -ball_speed[0]
				ball_speed[1] = -ball_speed[1] * bat_slow_coef
				ball_pos[1] = bat_pos[1] - bat_height / 2 - radius
			elif bat_pos[0] + bat_width / 2 < ball_pos[0] < bat_pos[0] + bat_width / 2 + radius:
				ball_speed[0] = -ball_speed[0]
				ball_speed[1] = -ball_speed[1] * bat_slow_coef
				ball_pos[1] = bat_pos[1] - bat_height / 2 - radius
		
		for brick in bricks:
			if is_alive(brick):
				left = brick[0] * brick_width
				right = left + brick_width
				top = brick[1] * brick_height
				bottom = top + brick_height
				if near(ball_pos, [left, top], radius) or near(ball_pos, [left, bottom], radius) or near(ball_pos, [right, top], radius) or near(ball_pos, [right, bottom], radius):
					brick[2] = brick[2] - 1
					ball_speed[0] = -ball_speed[0] * math.sqrt(brick_fast_coef)
					ball_speed[1] = -ball_speed[1] * math.sqrt(brick_fast_coef)
					break
				elif in_rect(ball_pos, left, right, top - radius, top) or in_rect(ball_pos, left, right, bottom, bottom + radius):
					brick[2] = brick[2] - 1
					ball_speed[1] = -ball_speed[1] * brick_fast_coef
				elif in_rect(ball_pos, left - radius, left, top, bottom) or in_rect(ball_pos, right, right + radius, top, bottom):
					brick[2] = brick[2] - 1
					ball_speed[0] = -ball_speed[0] * brick_fast_coef
		
		# update rects
		batrect.center = bat_pos
		ballrect.center = ball_pos
		
		# draw and flip
		screen.fill(black)
		for brick in bricks:
			if is_alive(brick):
				screen.blit(ibricks[brick[2] - 1], get_brick_rect(brick))
		screen.blit(ibat, batrect)
		screen.blit(iball, ballrect)
		pygame.display.flip()

if __name__ == '__main__': main()